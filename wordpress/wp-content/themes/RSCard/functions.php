<?php
add_action('wp_enqueue_scripts', 'custom_scripts');
function custom_scripts() {
  // Google fonts
  wp_enqueue_style('fredoka-one', 'https://fonts.googleapis.com/css?family=Fredoka+One');
  wp_enqueue_style('open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic');
  // Icon fonts
  wp_enqueue_style('map-icons', get_template_directory_uri() . '/fonts/map-icons/css/map-icons.min.css');
  wp_enqueue_style('icomoon', get_template_directory_uri() . '/fonts/icomoon/style.css');
  // Styles
  wp_enqueue_style('bx-slider', get_template_directory_uri() . '/js/plugins/jquery.bxslider/jquery.bxslider.css');
  wp_enqueue_style('custom-scrollbar', get_template_directory_uri() . '/js/plugins/jquery.customscroll/jquery.mCustomScrollbar.min.css');
  wp_enqueue_style('mediaelement-player', get_template_directory_uri() . '/js/plugins/jquery.mediaelement/mediaelementplayer.min.css');
  wp_enqueue_style('fancybox', get_template_directory_uri() . '/js/plugins/jquery.fancybox/jquery.fancybox.css');
  wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/js/plugins/jquery.owlcarousel/owl.carousel.css');
  wp_enqueue_style('owl-theme', get_template_directory_uri() . '/js/plugins/jquery.owlcarousel/owl.theme.css');
  wp_enqueue_style('option-panel', get_template_directory_uri() . '/js/plugins/jquery.optionpanel/option-panel.css');
  wp_enqueue_style('style', get_template_directory_uri() . '/style.css');
  wp_enqueue_style('theme-color', get_template_directory_uri() . '/colors/theme-color.css');
  // Scripts
  wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/libs/modernizr.js');
  wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js', array(), false, true);
  wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js', array(), false, true);
  wp_enqueue_script('js', get_template_directory_uri() . '/js/site.js', array(), false, true);
}

add_action('after_setup_theme', 'custom_theme_setup');
function custom_theme_setup() {
  add_theme_support('title-tag');
  add_theme_support('post-thumbnails');
}

add_action('pre_get_posts', 'pagination');
function pagination($query) {
  /*echo '<h1>';
  var_dump($query->is_main_query());
  echo '</h1>';*/
  if ($query->is_main_query()) {
      $query->set('posts_per_page', 10);
      $query->set('paged', (get_query_var('paged') ? get_query_var('paged') : 1));
  }
}

function popular_posts() {
  $the_query = new WP_Query(array( 'post_per_page' => 3, 'orderby' => 'comment_count'));
  return $the_query;
}