<?php get_header(); ?>

<!-- START: PAGE CONTENT -->			
<div class="row animate-up">
  <div class="col-sm-8">

    <?php if(have_posts()): while(have_posts()): the_post(); ?>
      <?php get_template_part('content-page'); ?>
    <?php endwhile; endif; ?>

  </div>
</div><!-- .row -->			
<!-- END: PAGE CONTENT -->

<?php get_footer(); ?>