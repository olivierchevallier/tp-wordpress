<?php get_header(); ?>
<!-- START: PAGE CONTENT -->
<div class="blog">
  <div class="blog-grid">
    <div class="grid-sizer"></div>
			<?php while(have_posts()): the_post(); ?>
        <?php get_template_part('content'); ?>
      <?php endwhile; ?>
		</div> <!-- .blog-grid -->

    <div class="pagination">
      <?php the_posts_pagination(array(
        "prev_text" => '<i class="rsicon rsicon-chevron_left"></i>',
        "next_text" => '<i class="rsicon rsicon-chevron_right"></i>',
        "screen_reader_text" => ' '
        )); ?>
    </div><!-- .pagination -->
	</div>
<!-- END: PAGE CONTENT -->
<?php get_footer(); ?>