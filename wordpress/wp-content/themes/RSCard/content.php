<?php 
  $date = the_date('d/M', '', '', false);
  preg_match('/(.*)\/(.*)/', $date, $match);
  $day = $match[1];
  $month = $match[2];
?>
<div class="grid-item">
  <article class="post-box animate-up">
      <div class="post-media">
          <div class="post-image">
              <a href="<?php the_permalink(); ?>"><?php if(has_post_thumbnail()) { the_post_thumbnail('medium'); }?> </a>
          </div>
      </div>

      <div class="post-data">
          <time class="post-datetime" datetime="2015-03-13T07:44:01+00:00">
              <span class="day"><?php echo $day; ?></span>
              <span class="month"><?php echo $month; ?></span>
          </time>

          <div class="post-tag">
            <?php the_tags('', ' '); ?>
          </div>

          <h3 class="post-title">
              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </h3>

          <div class="post-info">
              <a href="category.html"><i class="rsicon rsicon-user"></i>by <?php the_author(); ?></a>
              <a href="category.html"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number(); ?></a>
          </div>
      </div>
  </article>
</div>