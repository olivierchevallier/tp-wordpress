<div class="sidebar sidebar-fixed">
  <button class="btn-sidebar btn-sidebar-close"> <i class="rsicon rsicon-close"></i></button>

    <div class="widget-area">
        <aside class="widget widget-profile">
            <div class="profile-photo">
                <img src="<?php echo get_template_directory_uri(); ?>/img/uploads/rs-photo-v2.jpg" alt="Robert Smith"/>
            </div>
            <div class="profile-info">
                <h2 class="profile-title"><?php the_author_meta('first_name', 1);?> <?php the_author_meta('last_name', 1); ?></h2>
                <h3 class="profile-position"><?php the_author_meta('description', 1); ?></h3>
            </div>
        </aside><!-- .widget-profile -->

        <aside class="widget widget_search">
            <h2 class="widget-title">Search</h2>
            <form class="search-form">
                <label class="ripple">
                    <span class="screen-reader-text">Search for:</span>
                    <input class="search-field" type="search" placeholder="Search">
                </label>
                <input type="submit" class="search-submit" value="Search">
            </form>
        </aside><!-- .widget_search -->

        <aside class="widget widget-popuplar-posts">
            <h2 class="widget-title">Popular posts</h2>
            <ul>
                <?php 
                $the_query = new WP_Query(array( 'post_per_page' => 3, 'orderby' => 'comment_count' ));
                
                while($the_query->have_posts()) : $the_query->the_post();
                ?>
                <li>
                    <div class="post-media"><a href="<?php the_permalink(); ?>"><?php if(has_post_thumbnail()) { the_post_thumbnail('small'); }?> </a></div>
                    <h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <div class="post-info"><a href="<?php the_permalink(); ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number(); ?> Comment(s)</a></div>
                </li>
                <?php endwhile; ?>
            </ul>
        </aside><!-- .widget-popuplar-posts -->

        <aside class="widget widget_tag_cloud">
            <h2 class="widget-title">Tag Cloud</h2>
            <div class="tagcloud">
            <?php foreach(get_tags() as $tag): ?>
                <a href="index.html" title="1 topic"><?php echo $tag->name; ?></a>
            <?php endforeach; ?>
            </div>
        </aside><!-- .widget_tag_cloud -->

        <aside class="widget widget-recent-posts">
            <h2 class="widget-title">Recent posts</h2>
            <ul>
                <?php 
                $the_query = new WP_Query(array( 'post_per_page' => 3, 'orderby' => 'date'));
                while($the_query->have_posts()) : $the_query->the_post();
                ?>
                    <li>
                        <div class="post-tag">
                            <?php the_tags('', ' '); ?>
                        </div>
                        <h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <div class="post-info"><a href="<?php the_permalink(); ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number(); ?> Comment(s)</a></div>
                    </li>
                <?php
                endwhile;
                ?>
            </ul>
        </aside><!-- .widget-recent-posts -->

        <aside class="widget widget_categories">
            <h2 class="widget-title">Categories</h2>
            <ul>
                <?php 
                $categories = get_categories(array(
                    'orderby' => 'name',
                    'order' => 'ASC'
                ));
                foreach($categories as $category) :
                ?>
                <li><a href="index.html" title="Architecture Category Posts"><?php echo $category->name; ?></a> (<?php echo $category->count ?>)</li>
                <?php endforeach; ?>
            </ul>
        </aside><!-- .widget_categories -->
    </div><!-- .widget-area -->
</div><!-- .sidebar -->