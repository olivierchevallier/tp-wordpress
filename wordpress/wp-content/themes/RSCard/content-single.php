<?php 
  $date = the_date('d/M', '', '', false);
  preg_match('/(.*)\/(.*)/', $date, $match);
  $day = $match[1];
  $month = $match[2];
?>


<article class="post-content section-box">		
  <div class="post-media">
    <?php if(has_post_thumbnail()) { the_post_thumbnail(); }?>							
  </div><!-- .post-media -->					
  <div class="post-inner">
    <header class="post-header">
      <div class="post-data">
        <div class="post-tag">
          <a href="single.html">#Photo</a>
          <a href="single.html">#Architect</a>
        </div>

        <div class="post-title-wrap">
          <h1 class="post-title"><?php the_title(); ?></h1>
          <time class="post-datetime" datetime="<?php the_date(); ?>">
            <span class="day"><?php echo $day; ?></span>
            <span class="month"><?php echo $month; ?></span>
          </time>
        </div>

        <div class="post-info">
          <a href="single.html"><i class="rsicon rsicon-user"></i>by <?php the_author(); ?></a>
          <a href="single.html"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number(); ?></a>
        </div>
      </div>
    </header>

    <div class="post-editor clearfix">
      <?php the_content(); ?>
    </div>

    <footer class="post-footer">
      <div class="post-share">
        <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-503b5cbf65c3f4d8" async="async"></script>
        <div class="addthis_sharing_toolbox"></div>
      </div>
    </footer>
  </div><!-- .post-inner -->
</article><!-- .post-content -->

<nav class="post-pagination section-box">
<?php 
  $previous = get_previous_post();
  $next = get_next_post();
?>
  <div class="post-next">
    <div class="post-tag">Previous Article <a href="<?php echo get_the_permalink($previous); ?>"><?php $tags = get_the_tags($previous, '', ' '); foreach($tags as $tag) echo $tag->name; ?></a></div>
    <h3 class="post-title"><a href="<?php echo get_the_permalink($previous); ?>"><?php echo get_the_title($previous) ?></a></h3>

    <div class="post-info">
      <a href="<?php echo get_the_permalink($previous); ?>"><i class="rsicon rsicon-user"></i>by <?php echo get_the_author($previous) ?></a>
      <a href="<?php echo get_the_permalink($previous); ?>"><i class="rsicon rsicon-comments"></i>56</a>
    </div>
  </div>
  <div class="post-prev">
    <div class="post-tag">Next Article <a href="<?php echo get_the_permalink($next); ?>"><?php $tags = get_the_tags($next, '', ' '); foreach($tags as $tag) echo $tag->name; ?></a></div>
    <h3 class="post-title"><a href="<?php echo get_the_permalink($next); ?>"><?php echo get_the_title($next) ?></a></h3>

    <div class="post-info">
      <a href="<?php echo get_the_permalink($next); ?>"><i class="rsicon rsicon-user"></i>by <?php echo get_the_author($next) ?></a>
      <a href="<?php echo get_the_permalink($next); ?>"><i class="rsicon rsicon-comments"></i>56</a>
    </div>
  </div>
</nav><!-- .post-pagination -->

<div class="post-comments">
  <h2 class="section-title">Comments (<?php echo get_comments_number() ?>)</h2>
  
  <?php if(get_comments_number() > 0): ?>
  <div class="section-box">
    <ol class="comment-list">
      <?php foreach(get_comments() as $comment) : ?>
      
        <li class="comment">
          <article class="comment-body">
            <img class="comment-avatar" src="img/rs-avatar-64x64.jpg" alt="avatar"/>
            <div class="comment-content">
              <div class="comment-meta">
                <span class="name"><?php echo $comment->comment_author; ?></span>
                <time class="date" datetime="<?php echo $comment->comment_date; ?>"><?php echo $comment->comment_date; ?></time>
                <a class="reply-link" href="single.html#comment-reply">Reply</a>
              </div>
              <div class="comment-message">
                <p><?php echo $comment->comment_content; ?></p>
              </div>
            </div>
          </article>
        </li><!-- .comment -->
      <?php endforeach; ?>
    </ol><!-- .comment-list -->

    <div id="comment-reply" class="comment-reply">
      <form>
        <div class="input-field">
          <input type="text" name="rs-comment-name"/>
          <span class="line"></span>
          <label>Name *</label>
        </div>

        <div class="input-field">
          <input type="email" name="rs-comment-email"/>
          <span class="line"></span>
          <label>Email *</label>
        </div>

        <div class="input-field">
          <input type="text" name="rs-comment-website"/>
          <span class="line"></span>
          <label>Website</label>
        </div>

        <div class="input-field">
          <textarea rows="4" name="rs-comment-message"></textarea>
          <span class="line"></span>
          <label>Type Comment Here *</label>
        </div>

        <div class="text-right">
          <span class="btn-outer btn-primary-outer ripple">
            <input class="btn btn-lg btn-primary" type="button" value="Leave Comment">
          </span>
        </div>
      </form>
    </div><!-- .comment-reply -->
  </div><!-- .section-box -->
  <?php endif; ?>
</div><!-- .post-comments -->